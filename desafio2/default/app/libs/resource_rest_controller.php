<?php

/**
 * Controlador para manejar peticiones REST
 * 
 * Por defecto cada acción se llama como el método usado por el cliente
 * (GET, POST, PUT, DELETE, OPTIONS, HEADERS, PURGE...)
 * ademas se puede añadir mas acciones colocando delante el nombre del método
 * seguido del nombre de la acción put_cancel, post_reset...
 *
 * @category Kumbia
 * @package Controller
 * @author kumbiaPHP Team
 */
require_once CORE_PATH . 'kumbia/kumbia_rest.php';
abstract class ResourceRestController extends RestController
{

  public function getAll()
  {
    $this->data = Utils::fetch_model($this->controller_name);
  }

  public function put()
  {
    try {
      $data = RestController::parseJSON(file_get_contents('php://input'));
      $this->data = ['ok' => (new $this->controller_name)->save($data)];
    } catch (\Throwable $th) {
      $this->error("There are some errors ", 400, ['response' =>  $th->getMessage(), 'ok' => false]);
    }
  }

  public function patch($id)
  {
    try {
      $data = RestController::parseJSON(file_get_contents('php://input'));
      $found = (new $this->controller_name)->find($id);
      if ($found->update($data))
        $this->data = ['ok' => true];
    } catch (\Throwable $th) {
      $this->error("There are some errors ", 400, ['response' => 'Not found resource', 'ok' => false]);
    }
  }

  public function delete($id)
  {

    if (((new $this->controller_name)->find($id)))
      if ((new $this->controller_name)->delete($id))
        $this->data = ['ok' => true];
      else
        $this->error("There are some errors ", 400, ['response' => 'Not found resource', 'ok' => false]);
  }

  public function get($id)
  {
    $this->data = (new $this->controller_name)->find($id);

    if (!$this->data)
      $this->error("There are some errors ", 400, ['response' => 'Not found resource', 'ok' => false]);
  }

  public function post_csv()
  {
    View::select(null, 'csv');
    $this->data = (new $this->controller_name)->find();
  }

  public function get_paginate(int $per_page = 5, int $page = 1)
  {
    $this->data = (new $this->controller_name)->paginate("page: $page", "per_page: $per_page");
  }

  public function get_fields()
  {
    $this->data = (new $this->controller_name)->_data_type;
  }
}
